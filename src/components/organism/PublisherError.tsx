import React from 'react'
import { OtPublisherContext } from '../../context/OtPublisher'

export const PublisherError = () => {
  const { publisherError } = React.useContext(OtPublisherContext)

  if (publisherError) {
    return (
      <div>
        {(publisherError as any).code === 1500 ? (
          <h1>Your browser denied permission to hardware devices</h1>
        ) : (
          <h1>Oooops! Something went wrong</h1>
        )}
      </div>
    )
  }
  return null
}
