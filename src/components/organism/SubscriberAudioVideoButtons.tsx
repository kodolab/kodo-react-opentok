import React from 'react'
import { OtSubscriberContext } from '../../context/OtSubscriber'
import { AudioVideoButtons } from '../molecules/AudioVideoButtons'

interface Props {
  isPublisher?: boolean
}

export const SubscriberAudioVideoButtons: React.FC<Props> = (props) => {
  const subscriberCtx = React.useContext(OtSubscriberContext)
  const { isPublisher } = props

  return (
    <AudioVideoButtons
      isPublisher={isPublisher}
      {...{
        audio: subscriberCtx.subscribeToAudio,
        video: subscriberCtx.subscribeToVideo,
        setAudio: subscriberCtx.setSubscribeToAudio,
        setVideo: subscriberCtx.setSubscribeToVideo
      }}
    />
  )
}
