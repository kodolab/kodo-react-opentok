import React from 'react'
import { OtPublisherContext } from '../../context/OtPublisher'

import IconButton from '@mui/material/IconButton'
import FlipCameraIos from '@mui/icons-material/FlipCameraIosOutlined'

export const PublisherSwitchCamera = () => {
  const publisherCtx = React.useContext(OtPublisherContext)

  if (publisherCtx.availableCameras.length > 1) {
    return (
      <IconButton
        style={{ color: 'white' }}
        // color='primary'
        onClick={publisherCtx.cycleVideo}
        size='large'
      >
        <FlipCameraIos />
      </IconButton>
    )
  } else {
    return null
  }
}
