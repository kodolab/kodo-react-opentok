import React from 'react'
import { PublisherAudioVideoButtons } from './PublisherAudioVideoButtons'
import { PublisherSwitchCamera } from './PublisherSwitchCamera'

interface Props {
  className?: string
}

export const PublisherControls: React.FC<Props> = (props) => {
  return (
    <div {...props}>
      <PublisherAudioVideoButtons />
      <PublisherSwitchCamera />
    </div>
  )
}
