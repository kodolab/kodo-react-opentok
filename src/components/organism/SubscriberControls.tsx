import React from 'react'
import { SubscriberAudioVideoButtons } from './SubscriberAudioVideoButtons'

interface Props {
  className?: string
  isPublisher?: boolean
}

export const SubscriberControls: React.FC<Props> = (props) => {
  return (
    <div {...props}>
      <SubscriberAudioVideoButtons isPublisher={props?.isPublisher} />
    </div>
  )
}
