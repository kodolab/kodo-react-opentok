import React from 'react'
import { OtSubscriber, OtSubscriberProps } from '../../context/OtSubscriber'
import { SubscriberControls } from './SubscriberControls'
import { Theme } from '@mui/material/styles'

import makeStyles from '@mui/styles/makeStyles'

const useStyles = makeStyles<Theme, SubscriberProps>((theme) => ({
  root: {
    position: 'relative',
    overflow: 'hidden',

    '& > div.fullscreen': {
      height: '100%'
    },
    '&:hover $buttonsContainer': {
      opacity: 1,
      bottom: 0
    },
    // '&:hover $fullScreenIcon': {
    //   opacity: 1
    // },
    width: (props) => props.width || 450,
    height: (props) => props.height || 360,
    margin: 'auto'
  },

  buttonsContainer: {
    position: 'absolute',
    bottom: -42,
    backgroundColor: '#ffffff40',
    width: '100%',
    opacity: 0,
    display: 'flex',
    justifyContent: 'center',

    transition: theme.transitions.create(['opacity', 'bottom'], {
      duration: 300
    })
  }
}))

export interface SubscriberProps extends OtSubscriberProps {
  width?: string | number
  height?: string | number
}

export const Subscriber: React.FC<SubscriberProps> = (props) => {
  const classes = useStyles(props)
  const { width, height, ...rest } = props

  return (
    <div className={classes.root}>
      <OtSubscriber {...rest}>
        <SubscriberControls className={classes.buttonsContainer} />
      </OtSubscriber>
    </div>
  )
}
