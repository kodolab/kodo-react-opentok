import { Theme } from '@mui/material/styles'
import makeStyles from '@mui/styles/makeStyles'
import React from 'react'
import { OtPublisher, OtPublisherProps } from '../../context/OtPublisher'
// import { PublisherAudioVideoButtons } from './PublisherAudioVideoButtons'
import { PublisherControls } from './PublisherControls'
import { PublisherError } from './PublisherError'

const useStyles = makeStyles<Theme, PublisherProps>((theme) => ({
  root: {
    position: 'relative',
    overflow: 'hidden',
    '& > div.fullscreen': {
      height: '100%'
    },
    '&:hover $buttonsContainer': {
      opacity: 1,
      bottom: 0
    },
    // '&:hover $fullScreenIcon': {
    //   opacity: 1
    // },
    width: (props) => props.width || 450,
    height: (props) => props.height || 360,
    margin: 'auto'
  },

  buttonsContainer: {
    position: 'absolute',
    bottom: -42,
    backgroundColor: '#ffffff40',
    width: '100%',
    opacity: 0,
    display: 'flex',
    justifyContent: 'center',

    transition: theme.transitions.create(['opacity', 'bottom'], {
      duration: 300
    })
  }
}))

export interface PublisherProps extends OtPublisherProps {
  width?: string | number
  height?: string | number
}

export const Publisher: React.FC<PublisherProps> = (props) => {
  const classes = useStyles(props)
  const { width, height, ...rest } = props
  return (
    <div className={classes.root}>
      <OtPublisher {...rest}>
        <PublisherError />
        <PublisherControls className={classes.buttonsContainer} />
      </OtPublisher>
    </div>
  )
}
