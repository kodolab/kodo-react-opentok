import React from 'react'
import { OtPublisherContext } from '../../context/OtPublisher'
import { AudioVideoButtons } from '../molecules/AudioVideoButtons'

export const PublisherAudioVideoButtons = () => {
  const publisherCtx = React.useContext(OtPublisherContext)

  return (
    <AudioVideoButtons
      isPublisher
      {...{
        audio: publisherCtx.publishAudio,
        video: publisherCtx.publishVideo,
        setAudio: publisherCtx.setPublishAudio,
        setVideo: publisherCtx.setPublishVideo
      }}
    />
  )
}
