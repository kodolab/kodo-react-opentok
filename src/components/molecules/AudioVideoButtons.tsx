import Checkbox from '@mui/material/Checkbox'
import FormControlLabel from '@mui/material/FormControlLabel'
import VideocamIcon from '@mui/icons-material/Videocam'
import VideocamOffIcon from '@mui/icons-material/VideocamOff'
import VolumeOffIcon from '@mui/icons-material/VolumeOff'
import VolumeUpIcon from '@mui/icons-material/VolumeUp'
import MicIcon from '@mui/icons-material/Mic'
import MicOffIcon from '@mui/icons-material/MicOff'
import React from 'react'

interface AudioVideoButtonsProps {
  audio: boolean
  video: boolean
  isPublisher?: boolean
  setAudio: (val: boolean) => void
  setVideo: (val: boolean) => void
}

export const AudioVideoButtons: React.FC<AudioVideoButtonsProps> = (props) => {
  const { audio, setAudio, video, setVideo, isPublisher } = props

  return (
    <React.Fragment>
      <FormControlLabel
        label=''
        control={
          <Checkbox
            icon={<VideocamOffIcon />}
            checkedIcon={<VideocamIcon />}
            name='checkedVideo'
            checked={video}
            onChange={(event) => setVideo(event.target.checked)}
          />
        }
      />
      <FormControlLabel
        label=''
        control={
          <Checkbox
            icon={isPublisher ? <MicOffIcon /> : <VolumeOffIcon />}
            checkedIcon={isPublisher ? <MicIcon /> : <VolumeUpIcon />}
            name='checkedAudio'
            checked={audio}
            color='secondary'
            onChange={(event) => setAudio(event.target.checked)}
          />
        }
      />
    </React.Fragment>
  )
}
