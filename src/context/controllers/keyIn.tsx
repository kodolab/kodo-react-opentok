import Immutable from 'immutable'

export function keyIn(...keys: string[]) {
  var keySet = Immutable.Set(keys)
  return function (_v: any, k: string) {
    return keySet.has(k)
  }
}
