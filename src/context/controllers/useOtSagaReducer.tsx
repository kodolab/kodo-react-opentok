/* import {
  Dispatch // useCallback
} from 'react'
import {
  openTokReducer,
  openTokInitialState,
  openTokStateType,
  Action
} from './useOtReducer'
import useSagaReducer from 'use-saga-reducer'
import {
  // END
  Saga
} from 'redux-saga'
// import { startSaga } from './useOpenTokSessionCtrl';

export const useOtSagaReducer: <S extends Saga<never[]>>(
  name: string,
  startSaga: S
) => [openTokStateType, Dispatch<Action>] = (name, startSaga) => {
  return useSagaReducer(startSaga, openTokReducer(name), openTokInitialState)
}
 */