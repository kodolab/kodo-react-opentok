import React from 'react'
import { useOtPublishers } from '../OtPublishers'
// import { useOtStreams } from '../OtStreams'
import { v4 } from 'uuid'
import { useVideoRefOrLocal } from './useVideoRefOrLocal'
import { useStateUpdate } from '../../hooks/useStateUpdate'

export interface OtPublisherContextType {
  publishVideo: boolean
  publishAudio: boolean
  setPublishVideo: (val: boolean) => void
  setPublishAudio: (val: boolean) => void
  publisherError: any
  availableCameras: OT.Device[]
  cycleVideo: () => void
}

export interface OtPublisherProps {
  audioBitrate?: number
  audioFallbackEnabled?: boolean
  autoGainControl?: boolean
  disableAudioProcessing?: boolean
  echoCancellation?: boolean
  enableStereo?: boolean
  fitMode?: 'cover' | 'contain'
  frameRate?: 30 | 15 | 7 | 1
  width?: string | number
  height?: string | number
  insertDefaultUI?: boolean
  insertMode?: 'replace' | 'after' | 'before' | 'append'
  maxResolution?: { width: number; height: number }
  mirror?: boolean
  name?: string
  noiseSuppression?: boolean
  resolution?:
    | '1280x960'
    | '1280x720'
    | '640x480'
    | '640x360'
    | '320x240'
    | '320x180'
    | undefined
  showControls?: boolean

  publishVideo?: boolean
  publishAudio?: boolean
  videoRef?: React.MutableRefObject<HTMLDivElement | null>
  onClose?: () => void
  onPublish?: (publisher: OT.Publisher, error?: OT.OTError | undefined) => void
  audioSource?: string | null | boolean | MediaStreamTrack
  videoSource?: string | null | boolean | MediaStreamTrack
}

export const useOpenTokPublisherCtrl = (props: OtPublisherProps) => {
  const { videoRef: videoRefFromProps, onClose, onPublish } = props

  const { videoRef, isLocalVideoRef } = useVideoRefOrLocal(videoRefFromProps)

  // define if publisher is published to session
  // const [publishStream] = React.useState(true)
  const [
    publisherError
    // setPublisherError
  ] = React.useState<OT.OTError | undefined>(undefined)
  const [publisherUUID] = React.useState(v4())

  const {
    publishVideo,
    publishAudio,
    setPublishVideo,
    setPublishAudio,
    availableCameras
  } = usePublisherOptions(props)

  const ctx = useOtPublishers()
  // const {session, isSessionConnected} = useOtSessionState()
  // const streams = useOtStreams()

  // console.log('streams', streams.handleStreamCreated())

  const currPublisher: OT.Publisher = React.useMemo(() => {
    return ctx.state.publishers[publisherUUID]
  }, [ctx.state.publishers, publisherUUID])

  // const publisherHandler = React.useMemo(() => {
  //   return {
  //     streamCreated: streams.handleStreamCreated,
  //     streamDestroyed: streams.handleStreamDestroyed
  //     // audioLevelUpdated: (level: any) => console.log('audioLevelUpdated', level)
  //   }
  // }, [])

  // connect local handler
  // React.useEffect(() => {
  //   if (!currPublisher) return
  //   // console.log('connect publisherHandler to ', currPublisher)
  //   currPublisher.on(publisherHandler)

  //   // return () =>{
  //   //     currPublisher.off(publisherHandler)
  //   // }
  // }, [currPublisher, publisherHandler])

  React.useEffect(() => {
    // console.log('OtPublisher videoRef', videoRef)

    if (!videoRef.current) {
      throw new Error('OtPublisher missing video ref')
    }

    if (!currPublisher) {
      const publisherOptions = {
        name: props.name || publisherUUID,
        showControls: props.showControls || false,
        publishAudio: publishAudio,
        publishVideo: publishVideo,
        // videoSource: 'screen' === 'screen' ? 'screen' : undefined,
        width: props.width || '100%',
        height: props.height || '100%',
        audioBitrate: props.audioBitrate,
        audioFallbackEnabled: props.audioFallbackEnabled,
        autoGainControl: props.autoGainControl,
        disableAudioProcessing: props.disableAudioProcessing,
        echoCancellation: props.echoCancellation,
        enableStereo: props.enableStereo,
        fitMode: props.fitMode,
        frameRate: props.frameRate,
        insertDefaultUI: props.insertDefaultUI,
        insertMode: props.insertMode || 'append',
        maxResolution: props.maxResolution,
        mirror: props.mirror,
        noiseSuppression: props.noiseSuppression,
        resolution: props.resolution,
        videoSource: props.videoSource,
        audioSource: props.audioSource
      }

      ctx.actions.publisherRequest({
        uid: publisherUUID,
        targetElement: videoRef.current,
        options: publisherOptions,
        onPublish
      })
    }
    // return () => {
    //   ctx.actions.publisherDestroyRequest({
    //     name: publisherUUID
    //   })
    // }
  }, [currPublisher])

  React.useEffect(() => {
    return () => {
      ctx.actions.publisherDestroyRequest({
        uid: publisherUUID
      })
    }
  }, [])

  // update video
  useUpdatePublisher(currPublisher, 'publishVideo', publishVideo)
  // update audio
  useUpdatePublisher(currPublisher, 'publishAudio', publishAudio)

  React.useEffect(() => {
    if (publishAudio === false && publishVideo === false) {
      onClose && onClose()
    }
  }, [publishAudio, publishVideo, onClose])

  const cycleVideo = React.useCallback(() => {
    currPublisher.cycleVideo()
  }, [currPublisher])

  const context = {
    publisher: currPublisher,
    publishVideo,
    publishAudio,
    setPublishVideo,
    setPublishAudio,
    publisherError,
    availableCameras,
    cycleVideo
  }

  // console.log('useOpenTokPublisherCtrl context', context)

  return {
    context,
    videoRef,
    isLocalVideoRef
  }
}

export const usePublisherOptions = (props: OtPublisherProps) => {
  const [availableCameras, setAvailableCameras] = React.useState<OT.Device[]>(
    []
  )
  const [publishVideo, setPublishVideo] = useStateUpdate<boolean>(
    props.publishVideo,
    true
  )
  const [publishAudio, setPublishAudio] = useStateUpdate<boolean>(
    props.publishAudio,
    true
  )

  React.useEffect(() => {
    OT.getDevices((_error, devices) => {
      const cameras = devices?.filter((d) => d.kind === 'videoInput') || []
      setAvailableCameras(cameras)
    })
  }, [])

  return {
    publishVideo,
    publishAudio,
    setPublishVideo,
    setPublishAudio,
    availableCameras
  }
}

export const useUpdatePublisher = (
  publisher: OT.Publisher | undefined,
  methodName: string,
  value: any
) => {
  React.useEffect(() => {
    if (!publisher) return
    publisher[methodName](value)
  }, [value])
}
