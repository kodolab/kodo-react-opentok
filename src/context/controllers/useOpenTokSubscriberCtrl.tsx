import React from 'react'
import { useVideoRefOrLocal } from './useVideoRefOrLocal'
import { useOtSubscribers } from '../OtSubscribers'
import { useStateUpdate } from '../../hooks/useStateUpdate'
import { v4 } from 'uuid'
import { Dimensions, SubscriberProperties } from '@opentok/client'

export interface OtSubscriberContextType {
  subscriber?: OT.Subscriber

  subscribeToVideo: boolean
  setSubscribeToVideo: (val: boolean) => void

  subscribeToAudio: boolean
  setSubscribeToAudio: (val: boolean) => void

  showControls: boolean
  setShowControls: (val: boolean) => void

  preferredFrameRate: number
  setPreferredFrameRate: (val: number) => void

  restrictFrameRate: boolean
  setRestrictFrameRate: (val: boolean) => void
}

export interface OtSubscriberProps extends SubscriberProperties {
  stream: OT.Stream
  videoRef?: React.MutableRefObject<HTMLDivElement | null>
  // subscribeToAudio?: boolean
  // subscribeToVideo?: boolean
  // showControls?: boolean
  // preferredFrameRate?: number
  restrictFrameRate?: boolean
  backgroundImageURI?: string
  callback?: ((error?: OT.OTError | undefined) => void) | undefined
  onHaveSubscribe?: (subscriber: OT.Subscriber) => void
  on?: OT.Subscriber['on'] | OT.Subscriber['on'][]
  once?: OT.Subscriber['once'] | OT.Subscriber['once'][]
}
export const useSubscriberOptions = (props: OtSubscriberProps) => {
  const [subscribeToVideo, setSubscribeToVideo] = useStateUpdate<boolean>(
    props.subscribeToVideo,
    true
  )
  const [subscribeToAudio, setSubscribeToAudio] = useStateUpdate(
    props.subscribeToAudio,
    false
  )
  const [showControls, setShowControls] = useStateUpdate(
    props.showControls,
    false
  )
  const [restrictFrameRate, setRestrictFrameRate] = useStateUpdate(
    props.restrictFrameRate,
    false
  )
  const [preferredFrameRate, setPreferredFrameRate] = useStateUpdate<number>(
    props.preferredFrameRate,
    25
  )

  const [preferredResolution, setPreferredResolution] = useStateUpdate<
    undefined | Dimensions
  >(props.preferredResolution, undefined)

  const [backgroundImageURI, setBackgroundImageURI] = useStateUpdate<
    string | undefined
  >(props.backgroundImageURI, undefined)

  return {
    subscribeToVideo,
    setSubscribeToVideo,

    subscribeToAudio,
    setSubscribeToAudio,

    showControls,
    setShowControls,

    preferredFrameRate,
    setPreferredFrameRate,

    preferredResolution,
    setPreferredResolution,

    restrictFrameRate,
    setRestrictFrameRate,

    backgroundImageURI,
    setBackgroundImageURI
  }
}

export const useUpdateSubscriber = (
  subscriber: OT.Subscriber | undefined,
  methodName: string,
  value: any,
  updateIfUndefined: boolean = true
) => {
  React.useEffect(() => {
    if (!subscriber) return
    if (value === undefined && !updateIfUndefined) return
    subscriber[methodName](value)
  }, [value])
}

export const useOpenTokSubscriberCtrl = (props: OtSubscriberProps) => {
  const [subscriberUUID] = React.useState(v4())
  const {
    videoRef: videoRefFromProps,
    stream,
    onHaveSubscribe,
    on: onCallbacks,
    once: onceCallbacks,
    ...subscriberProperties
  } = props
  const { videoRef, isLocalVideoRef } = useVideoRefOrLocal(videoRefFromProps)

  // console.log('OtSubscriber stream', stream)
  // const { session, isSessionConnected } = useOtSessionState()
  const {
    subscriberPlaceholderImageUriList,
    state: { subscribersMap },
    actions: {
      requestSubscriber,
      requestDestroySubscriber
      // addSubscriber,
      // removeSubscriber
    }
  } = useOtSubscribers()

  // console.log('OtSubscriber subscribersMap', subscribersMap)
  const currSubscriber = React.useMemo(() => {
    return subscribersMap[subscriberUUID]
  }, [subscribersMap, subscriberUUID])

  React.useEffect(() => {
    if (currSubscriber && onHaveSubscribe) {
      onHaveSubscribe(currSubscriber)
    }
  }, [currSubscriber])

  // Connect on end off callback
  React.useEffect(() => {
    if (!currSubscriber) return
    if (!onCallbacks) return

    if (Array.isArray(onCallbacks)) {
      onCallbacks.map((callbackConf) => {
        currSubscriber.on(callbackConf)
      })
      return () => {
        onCallbacks.map((callbackConf) => {
          currSubscriber.off(callbackConf)
        })
      }
    } else {
      currSubscriber.on(onCallbacks)
      return () => {
        currSubscriber.off(onCallbacks)
      }
    }
  }, [currSubscriber, onCallbacks])

  // Connect once callback
  React.useEffect(() => {
    if (!currSubscriber) return
    if (!onceCallbacks) return
    if (Array.isArray(onceCallbacks)) {
      onceCallbacks.map((callbackConf) => {
        currSubscriber.once(callbackConf)
      })
    } else {
      currSubscriber.once(onceCallbacks)
    }
  }, [currSubscriber, onceCallbacks])

  const defaultBackgroundImageURI = React.useMemo(() => {
    if (
      !subscriberPlaceholderImageUriList ||
      !subscriberPlaceholderImageUriList.length
    ) {
      return
    }
    return subscriberPlaceholderImageUriList[
      Math.floor(Math.random() * subscriberPlaceholderImageUriList.length)
    ]
  }, [subscriberPlaceholderImageUriList])

  const options = useSubscriberOptions(props)

  React.useEffect(() => {
    const initialProperties = {
      ...subscriberProperties,
      width: props.width || '100%',
      height: props.height || '100%',
      showControls: options.showControls,
      subscribeToAudio: options.subscribeToAudio,
      subscribeToVideo: options.subscribeToVideo,
      ...(options.backgroundImageURI || defaultBackgroundImageURI
        ? {
            style: {
              backgroundImageURI:
                options.backgroundImageURI || defaultBackgroundImageURI
            }
          }
        : {})
    }
    // console.log('initialProperties', initialProperties)
    requestSubscriber({
      subscriberUUID,
      stream,
      ...(videoRef.current ? { targetElement: videoRef.current } : {}),
      properties: initialProperties,
      callback: props.callback
    })

    return () => {
      requestDestroySubscriber({ stream, subscriberUUID })
    }
  }, [])

  // update video
  useUpdateSubscriber(
    currSubscriber,
    'subscribeToVideo',
    options.subscribeToVideo
  )
  // update audio
  useUpdateSubscriber(
    currSubscriber,
    'subscribeToAudio',
    options.subscribeToAudio
  )

  // update restrictFrameRate
  useUpdateSubscriber(
    currSubscriber,
    'restrictFrameRate',
    options.restrictFrameRate
  )

  // update preferredFrameRate
  useUpdateSubscriber(
    currSubscriber,
    'setPreferredFrameRate',
    options.preferredFrameRate
  )

  // update setPreferredResolution
  useUpdateSubscriber(
    currSubscriber,
    'setPreferredResolution',
    options.preferredResolution,
    false
  )

  const context = {
    subscriber: currSubscriber,
    ...options
  }

  return {
    context,
    isLocalVideoRef,
    videoRef
  }
}
