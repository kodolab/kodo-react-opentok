import React, { useCallback } from 'react'
import { useOtSession } from '../OtSession'
import {
  ConnectionCreatedEvent,
  ConnectionDestroyedEvent,
  EventHandler
} from '../../types/opentok'
import useSessionEventHandler from './useSessionEventHandler'

export const useOpenTokConnectionsCtrl = () => {
  const session = useOtSession()
  const [connections, setConnections] = React.useState<OT.Connection[]>([])

  const connectionCreated = React.useCallback((connection: OT.Connection) => {
    setConnections((oldConnections) => [connection, ...oldConnections])
  }, [])

  const connectionDestroyed = React.useCallback((connection: OT.Connection) => {
    setConnections((oldConnections) =>
      oldConnections.filter(
        (x: OT.Connection) => x.connectionId === connection.connectionId
      )
    )
  }, [])

  const actions = React.useMemo(
    () => ({
      connectionCreated,
      connectionDestroyed
    }),
    []
  )

  /*  const session = useOtSession()

  const connections = state.get('connections')?.toArray() */

  const handleConnectionCreated: EventHandler<ConnectionCreatedEvent> = useCallback(
    (event) => {
      actions.connectionCreated(event.connection)
    },
    [actions]
  )

  const handleConnectionDestroyed: EventHandler<ConnectionDestroyedEvent> = useCallback(
    (event) => {
      actions.connectionDestroyed(event.connection)
    },
    [actions]
  )

  useSessionEventHandler('connectionCreated', handleConnectionCreated, session)
  useSessionEventHandler(
    'connectionDestroyed',
    handleConnectionDestroyed,
    session
  )

  return {
    state: {
      connections
    }
  }
}

export type useOpenTokConnectionsCtrlType = ReturnType<
  typeof useOpenTokConnectionsCtrl
>
