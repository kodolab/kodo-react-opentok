import React from 'react'
import { useOtSession } from '../OtSession'
import useSessionEventHandler from './useSessionEventHandler'
import omit from 'lodash/omit'

export const useOpenTokStreamsCtrl = () => {
  // console.log('OtStreams ')
  // const ctx = useOpenTokStreamsCtrl();
  const session = useOtSession()
  const [streamsMap, setStreamsMap] = React.useState<{
    [key: string]: OT.Stream
  }>({})

  const handleStreamCreated = React.useCallback((event) => {
    const { stream } = event
    // console.log('OtStreams handleStreamCreated', event)

    setStreamsMap((streamsMapOld) => ({
      ...streamsMapOld,
      [stream.id]: stream
    }))
    // actions.streamCreated(event.stream)
  }, [])

  const handleStreamDestroyed = React.useCallback((event) => {
    const { stream } = event
    // console.log('OtStreams handleStreamDestroyed', event)
    setStreamsMap((streamsMapOld) => omit(streamsMapOld, stream?.id))
  }, [])

  useSessionEventHandler('streamCreated', handleStreamCreated, session)
  useSessionEventHandler('streamDestroyed', handleStreamDestroyed, session)

  const streams = React.useMemo(() => {
    return Object.values(streamsMap)
  }, [streamsMap])

  return {
    state: {
      streams
    },
    handleStreamCreated,
    handleStreamDestroyed
  }
}

export type useOpenTokStreamsCtrlType = ReturnType<typeof useOpenTokStreamsCtrl>
