import React from 'react'
import { useOtSession } from '../OtSession'
import omit from 'lodash/omit'

type requestSubscriberType = (props: {
  subscriberUUID: string
  stream: OT.Stream
  targetElement?: string | HTMLElement | undefined
  properties?: OT.SubscriberProperties | undefined
  callback?: ((error?: OT.OTError | undefined) => void) | undefined
}) => void

type requestDestroySubscriberType = (props: {
  subscriberUUID: string
  stream: OT.Stream
}) => void

export const useOpenTokSubscribersCtrl = () => {
  const session = useOtSession()
  const [subscribersMap, setSubscribersMap] = React.useState<{
    [key: string]: OT.Subscriber
  }>({})

  const subscribers = React.useMemo(() => Object.values(subscribersMap), [
    subscribersMap
  ])

  // const removeSubscriber = React.useCallback(())

  const requestSubscriber = React.useCallback<requestSubscriberType>(
    ({ subscriberUUID, stream, targetElement, properties, callback }) => {
      if (!session) {
        throw new Error(
          'requestSubscriber requestDestroySubscriber missing session'
        )
      }

      const defaultCallback = (error: OT.OTError | undefined) => {
        if (error) {
          console.log('requestSubscriber subscribeToStream Errro', error)
        }
      }

      const target =
        properties?.insertDefaultUI === false ? undefined : targetElement

      const subscriber = session?.subscribe(
        stream,
        target,
        // targetElement,
        properties,
        // // initialProperties,
        // {
        //   insertDefaultUI:false
        // },
        callback || defaultCallback
      )

      // subscriber.on({
      //   disconnected: (e: any) => {
      //     // Display a user interface notification.
      //     console.log('requestSubscriber  disconnected', e)
      //   },
      //   connected: (e: any) => {
      //     // Adjust user interface.
      //     console.log('requestSubscriber connected', e)
      //   },
      //   destroyed: (e: any) => {
      //     // Adjust user interface.
      //     console.log('requestSubscriber destroyed', e)
      //   }
      // })

      if (subscriber) {
        setSubscribersMap((oldMap) => ({
          ...oldMap,
          [subscriberUUID]: subscriber
        }))
      }
    },
    [session]
  )

  const requestDestroySubscriber = React.useCallback<requestDestroySubscriberType>(
    ({ subscriberUUID, stream }) => {
      const subscriber = subscribersMap[subscriberUUID]
      if (!session) {
        throw new Error('requestDestroySubscriber missing session')
      }

      if (subscriber) {
        // console.log('requestDestroySubscriber delte subscriber', subscriber)
        session.unsubscribe(subscriber)
      } else {
        // console.log(
        //   'requestDestroySubscriber check subscribers for stream:',
        //   stream
        // )
        const subscribersToRemove = session.getSubscribersForStream(stream)
        // console.log(
        //   'requestDestroySubscriber subscribers_to_remove:',
        //   subscribersToRemove
        // )
        subscribersToRemove.forEach((s) => {
          // console.log('requestDestroySubscriber s:', s)
          session.unsubscribe(s)
        })
      }

      setSubscribersMap((oldMap) => omit(oldMap, subscriberUUID))
    },
    [session]
  )

  // console.log('subscribersMap', subscribersMap)
  // console.log('subscribers', subscribers)

  const actions = React.useMemo(
    () => ({
      requestSubscriber,
      requestDestroySubscriber
    }),
    [requestSubscriber, requestDestroySubscriber]
  )

  return {
    state: {
      subscribers,
      subscribersMap
    },
    actions
  }
}

export type useOpenTokSubscribersCtrlType = ReturnType<
  typeof useOpenTokSubscribersCtrl
>
