import React from 'react'
import { useOtSession } from '../OtSession'

import { useOtStreams } from '../OtStreams'
import omit from 'lodash/omit'
import OT, { PublisherProperties, OTError } from '@opentok/client'

export const OtPublishersContext = React.createContext<any>({})

type publisherRequestType = (props: {
  uid: string
  targetElement: string | HTMLElement
  options?: PublisherProperties
  callback?: (error?: OTError) => void
  onPublish?: (publisher: OT.Publisher, error?: OT.OTError | undefined) => void
}) => void

type publisherDestroyRequestType = (props: { uid: string }) => void

export const useGetDevices = () => {
  const [devices, setDevices] = React.useState<OT.Device[]>([])

  React.useEffect(() => {
    OT.getDevices((error, devices) => {
      if (error) {
        console.log('useGetDevices error, devices', error, devices)
      }
      if (devices) {
        setDevices(devices)
      }
    })
  }, [])

  const audioDevices = React.useMemo(() => {
    return devices.filter((d) => d.kind === 'audioInput')
  }, [devices])

  const videoDevices = React.useMemo(() => {
    return devices.filter((d) => d.kind === 'videoInput')
  }, [devices])

  // console.log('useGetDevices', {
  //   devices,
  //   audioDevices,
  //   videoDevices
  // })
  return {
    devices,
    audioDevices,
    videoDevices
  }
}

export const useOpenTokPublishersCtrl = () => {
  const session = useOtSession()
  const streams = useOtStreams()
  const [publishers, setPublishers] = React.useState<{
    [key: string]: OT.Publisher
  }>({})
  const [publisherRequests, setPublisherRequests] = React.useState<{
    [key: string]: {
      targetElement: string | HTMLElement
      options?: PublisherProperties
      callback?: (error?: OTError) => void
      onPublish?: (
        publisher: OT.Publisher,
        error?: OT.OTError | undefined
      ) => void
    }
  }>({})

  const removePublisher = React.useCallback((publisher: OT.Publisher) => {
    setPublishers((oldPublishers) => {
      // const publisher = oldPublishers[name]
      const match = Object.entries(oldPublishers).find(
        ([, p]) => p.id === publisher.id
      )
      if (!match) return oldPublishers

      const [key, p] = match
      p.destroy()
      p.off(publisherHandler)
      return omit(oldPublishers, key)
    })
  }, [])

  const publisherHandler = React.useMemo(() => {
    return {
      streamCreated: streams.handleStreamCreated,
      streamDestroyed: streams.handleStreamDestroyed,
      destroyed: (event: any) => {
        console.log(
          'useOpenTokPublishersCtrl  publisherHandler.destroyed',
          event
        )
        removePublisher(event.target)
      },
      videoElementCreated: (level: any) =>
        console.log(
          'useOpenTokPublishersCtrl publisherHandler.videoElementCreated',
          level
        )
      // 'audioLevelUpdated': (level:any) => console.log('audioLevelUpdated',level)
    }
  }, [])

  const publisherRequest = React.useCallback<publisherRequestType>(
    ({ uid, targetElement, options, callback, onPublish }) => {
      console.log(
        'useOpenTokPublishersCtrl publisherRequest',
        name,
        targetElement,
        options
      )
      setPublisherRequests((oldPublisherRequests) => ({
        ...oldPublisherRequests,
        [uid]: {
          targetElement,
          options,
          callback,
          onPublish
        }
      }))

      const initPubCb = (error: OT.OTError | undefined) => {
        // console.log('publish initPubCb callback', error)
        callback && callback(error)
      }

      const publisher = OT.initPublisher(targetElement, options, initPubCb)
      publisher.on(publisherHandler)

      // const pubCb = (error: OT.OTError | undefined) => {
      //   // console.log('publish callback', publisher, error)
      //   onPublish && onPublish(publisher, error)
      // }

      // if (!session) {
      //   console.error('Missing session')
      // }

      // session && session.publish(publisher, pubCb)

      setPublishers((oldPublishers) => ({
        ...oldPublishers,
        [uid]: publisher
      }))
    },
    [setPublishers, session, publisherHandler]
  )

  const publisherDestroyRequest = React.useCallback<publisherDestroyRequestType>(
    ({ uid }) => {
      console.log('useOpenTokPublishersCtrl publisherDestroyRequest', uid)
      setPublishers((oldPublishers) => {
        const publisher = oldPublishers[uid]
        if (!publisher) return oldPublishers
        publisher.destroy()
        publisher.off(publisherHandler)
        return omit(oldPublishers, uid)
      })
    },
    [setPublishers]
  )

  React.useEffect(() => {
    if (session && (session as any)?.isConnected()) {
      Object.entries(publishers).forEach(([uid, publisher]) => {
        // console.log('OtPublishers key, publisher', key, publisher)
        if (!publisher.stream || !publisher.session) {
          console.log(
            'useOpenTokPublishersCtrl TRY Reconnect publisher',
            publisher
          )
          // const pubCb = (error: OT.OTError | undefined) => {
          //   console.log('OtPublishers publish callback', publisher, error)
          // }

          const {
            onPublish
            // , options
          } = publisherRequests[uid]

          // session.publish(publisher, pubCb)
          const pubCb = (error: OT.OTError | undefined) => {
            // console.log('publish callback', publisher, error)
            onPublish && onPublish(publisher, error)
          }

          if (!session) {
            console.error('Missing session')
          }

          session && session.publish(publisher, pubCb)
          // session &&
          //   OT.getUserMedia(options).then((res) => {
          //     console.log('useGetUserMedia res', res)
          //     session.publish(publisher, pubCb)
          //   })
        }
      })
    }
  }, [publishers, session, (session as any)?.currentState])

  const availableDevices = useGetDevices()

  return {
    state: {
      publishers,
      availableDevices
    },
    actions: {
      publisherRequest,
      publisherDestroyRequest
    }
  }
}

export type useOpenTokPublishersCtrlType = ReturnType<
  typeof useOpenTokPublishersCtrl
>
