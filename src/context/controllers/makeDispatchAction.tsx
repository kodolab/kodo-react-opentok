import React from 'react'

export const makeDispatchAction: <A, S extends (...args: any[]) => A>(
  dispatch: React.Dispatch<A>,
  action: S
) => (...args: Parameters<S>) => void = (dispatch, action) => {
  return (...args) => dispatch(action(...args))
}
