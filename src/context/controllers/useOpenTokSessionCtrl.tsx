import * as React from 'react'
import OT from '@opentok/client'

import { useForceUpdate } from '../../hooks/useForceUpdate'

export type initSessionOptionsType = {
  connectionEventsSuppressed?: boolean
  iceConfig?: {
    includeServers: 'all' | 'custom'
    transportPolicy: 'all' | 'relay'
    customServers: {
      urls: string | string[]
      username?: string
      credential?: string
    }[]
  }
}

export interface OTSessionProps {
  apiKey: string
  sessionId: string
  token: string
  options?: initSessionOptionsType
}

const DEF_OPTIONS = {}
export const useOpenTokSessionCtrl = (props: OTSessionProps) => {
  const { apiKey, sessionId, token, options = DEF_OPTIONS } = props
  // const session = React.useRef<OT.Session | undefined>()
  const [session, setSession] = React.useState<OT.Session | undefined>()
  const update = useForceUpdate()

  const onSessionConnected = React.useCallback((e: any) => {
    console.log('useOpenTokSessionCtrl onSessionConnected', e)
    update()
  }, [])

  const onSessionDisconnected = React.useCallback((e: any) => {
    console.log('useOpenTokSessionCtrl onSessionDisconnected', e)
    update()
  }, [])

  const onSessionGenericEvents = React.useCallback((e: any) => {
    console.log('useOpenTokSessionCtrl onSessionGenericEvents', e)
    update()
  }, [])

  const sessionHandler = React.useMemo(() => {
    return {
      sessionConnected: onSessionConnected,
      sessionReconnected: onSessionGenericEvents,
      sessionDisconnected: onSessionDisconnected,
      sessionReconnecting: onSessionGenericEvents
    }
  }, [])

  const initSession = React.useCallback(() => {
    console.log('useOpenTokSessionCtrl initSession')
    const newSession = OT.initSession(apiKey, sessionId, options)
    newSession.on(sessionHandler)
    setSession(newSession)
  }, [apiKey, sessionId, token, options, sessionHandler])

  const destroySession = React.useCallback(() => {
    console.log('useOpenTokSessionCtrl destroySession')
    if (session) {
      session.off(sessionHandler)
      session.disconnect()
      // session = undefined
      setSession(undefined)
    }
  }, [session, sessionHandler])

  const connectSession = React.useCallback(() => {
    const connectionCb = (error: OT.OTError | undefined) => {
      // console.log('connectionCb', error)
      if (error) {
      }
      update()
    }

    if (session) {
      // console.log('useOpenTokSessionCtrl connectSession')
      session.connect(token, connectionCb)
    }
  }, [session, token])

  React.useEffect(() => {
    initSession()

    return () => {
      if (session) {
        destroySession()
      }
    }
  }, [])

  const sessionIsConnected = (session as any)?.isConnected()
  const sessionCurrentState = (session as any)?.currentState
  React.useEffect(() => {
    // console.log('sessionCurrentState', sessionCurrentState, sessionIsConnected)

    if (!sessionIsConnected) {
      connectSession()
    }
  }, [connectSession, sessionIsConnected, sessionCurrentState])

  return {
    state: {
      session: session
    }
  }
}

export type useOpenTokSessionCtrlType = ReturnType<typeof useOpenTokSessionCtrl>
