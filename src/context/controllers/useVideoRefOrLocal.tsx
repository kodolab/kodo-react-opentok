import React from 'react'

export const useVideoRefOrLocal = (
  videoRefFromProps?: React.MutableRefObject<HTMLDivElement | null>
) => {
  const localVideoRef = React.useRef(null)
  const { videoRef, isLocalVideoRef } = React.useMemo(() => {
    const videoRef = videoRefFromProps || localVideoRef
    return {
      videoRef,
      isLocalVideoRef: videoRef === localVideoRef
    }
  }, [])

  return { videoRef, isLocalVideoRef }
}
