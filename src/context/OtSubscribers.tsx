import React from 'react'
import {
  useOpenTokSubscribersCtrl,
  useOpenTokSubscribersCtrlType // ,useOpenTokType
} from './controllers/useOpenTokSubscribersCtrl'

type OtSubscribersContextType = useOpenTokSubscribersCtrlType & {
  subscriberPlaceholderImageUriList?: string[]
}

export const OtSubscribersContext =
  React.createContext<OtSubscribersContextType>({
    state: {
      streams: []
    }
  } as any)

interface OtSubscribersProps {
  subscriberPlaceholderImageUriList?: string[]
}
export const OtSubscribers: React.FC<OtSubscribersProps> = (props) => {
  const { subscriberPlaceholderImageUriList } = props

  const ctx = {
    ...useOpenTokSubscribersCtrl(),
    subscriberPlaceholderImageUriList
  }

  // console.log('subscribers', ctx.state.subscribers)
  // console.log('OtSubscribers ctx', ctx)

  return (
    <OtSubscribersContext.Provider value={ctx}>
      {props.children}
    </OtSubscribersContext.Provider>
  )
}

export const useOtSubscribers = () => {
  return React.useContext(OtSubscribersContext)
}
