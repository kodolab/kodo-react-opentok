import React from 'react'
import {
  useOpenTokSubscriberCtrl,
  OtSubscriberContextType,
  OtSubscriberProps
} from './controllers/useOpenTokSubscriberCtrl'

export { OtSubscriberProps }
export const OtSubscriberContext = React.createContext<OtSubscriberContextType>(
  {} as any
)

export const OtSubscriber: React.FC<OtSubscriberProps> = (props) => {
  const { context, isLocalVideoRef, videoRef } = useOpenTokSubscriberCtrl(props)

  return (
    <OtSubscriberContext.Provider value={context}>
      {isLocalVideoRef && <div ref={videoRef} />}
      {props.children}
    </OtSubscriberContext.Provider>
  )
}

export const useOtSubscriber = () => {
  return React.useContext(OtSubscriberContext)
}
