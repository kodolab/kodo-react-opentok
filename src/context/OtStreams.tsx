import React from 'react'

import {
  useOpenTokStreamsCtrl,
  useOpenTokStreamsCtrlType
} from './controllers/useOpenTokStreamsCtrl'

export const OtStreamsContext = React.createContext<useOpenTokStreamsCtrlType>({
  state: {
    streams: []
  }
} as any)

export const OtStreams: React.FC = (props) => {
  const ctx = useOpenTokStreamsCtrl()
  return (
    <OtStreamsContext.Provider value={ctx}>
      {props.children}
    </OtStreamsContext.Provider>
  )
}

export const useOtStreams = () => {
  return React.useContext(OtStreamsContext)
}
