import React from 'react'
import {
  useOpenTokConnectionsCtrlType,
  useOpenTokConnectionsCtrl // ,useOpenTokType
} from './controllers/useOpenTokConnectionsCtrl'

export const OtConnectionsContext =
  React.createContext<useOpenTokConnectionsCtrlType>({
    state: {
      connections: []
    }
  })

export const OtConnections: React.FC = (props) => {
  const ctx = useOpenTokConnectionsCtrl()

  return (
    <OtConnectionsContext.Provider value={ctx}>
      {props.children}
    </OtConnectionsContext.Provider>
  )
}

export const useOtConnections = () => {
  return React.useContext(OtConnectionsContext)
}
