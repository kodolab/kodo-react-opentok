import React from 'react'
import {
  OtPublisherContextType,
  OtPublisherProps,
  useOpenTokPublisherCtrl
} from './controllers/useOpenTokPublisherCtrl'

export { OtPublisherProps }

export const OtPublisherContext = React.createContext<OtPublisherContextType>(
  {} as any
)

export const OtPublisher: React.FC<OtPublisherProps> = (props) => {
  const { context, isLocalVideoRef, videoRef } = useOpenTokPublisherCtrl(props)
  return (
    <OtPublisherContext.Provider value={context}>
      {isLocalVideoRef && (
        <div
          style={{ width: '100%', height: '100%' }}
          className='OtPubCtx.videoRef'
          ref={videoRef}
        />
      )}
      {props.children}
    </OtPublisherContext.Provider>
  )
}

export const useOtPublisherContext = () => {
  return React.useContext(OtPublisherContext)
}
