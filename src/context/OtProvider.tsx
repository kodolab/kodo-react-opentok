import React from 'react'
import { OtSession, OTSessionProps } from './OtSession'
import { OtConnections } from './OtConnections'
import { OtStreams } from './OtStreams'
import { OtPublishers } from './OtPublishers'
import { OtSubscribers } from './OtSubscribers'

interface OtProviderProps extends OTSessionProps {
  subscriberPlaceholderImageUriList?: string[]
}

export const OtProvider: React.FC<OtProviderProps> = (props) => {
  const { children, subscriberPlaceholderImageUriList, ...rest } = props
  return (
    <OtSession {...rest}>
      <OtConnections>
        <OtStreams>
          <OtPublishers>
            <OtSubscribers
              subscriberPlaceholderImageUriList={
                subscriberPlaceholderImageUriList
              }
            >
              {children}
            </OtSubscribers>
          </OtPublishers>
        </OtStreams>
      </OtConnections>
    </OtSession>
  )
}
