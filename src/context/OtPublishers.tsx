import React from 'react'
import {
  useOpenTokPublishersCtrl,
  useOpenTokPublishersCtrlType
} from './controllers/useOpenTokPublishersCtrl'

export const OtPublishersContext =
  React.createContext<useOpenTokPublishersCtrlType>(
    {} as useOpenTokPublishersCtrlType
  )

export const OtPublishers: React.FC = (props) => {
  const ctx = useOpenTokPublishersCtrl()
  return (
    <OtPublishersContext.Provider value={ctx}>
      {props.children}
    </OtPublishersContext.Provider>
  )
}

export const useOtPublishers = () => {
  return React.useContext(OtPublishersContext)
}

export const useOtAvailableDevices = () => {
  const {
    state: { availableDevices }
  } = React.useContext(OtPublishersContext)
  return availableDevices
}
