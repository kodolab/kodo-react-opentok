import * as React from 'react'

import {
  useOpenTokSessionCtrl,
  useOpenTokSessionCtrlType,
  OTSessionProps
} from './controllers/useOpenTokSessionCtrl'

export { OTSessionProps }

export const OtSessionContext = React.createContext<useOpenTokSessionCtrlType>(
  {} as any
)

export const OtSession: React.FC<OTSessionProps> = (props) => {
  const ctx = useOpenTokSessionCtrl(props)

  return (
    <OtSessionContext.Provider value={ctx}>
      {props.children}
    </OtSessionContext.Provider>
  )
}

export const useOtSession = () => {
  const {
    state: { session }
  } = React.useContext(OtSessionContext)
  return session
}

export const useOtSessionState = () => {
  const { state } = React.useContext(OtSessionContext)
  // console.log('render useOtSessionState state diff:',useDifferences(state))
  return state
}

export const useOtSessionContext = () => {
  return React.useContext(OtSessionContext)
}
