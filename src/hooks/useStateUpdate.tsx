import React from 'react'

export function useStateUpdate<S>(
  propState: S | (() => S) | undefined,
  initialState: S | (() => S)
): [S, React.Dispatch<React.SetStateAction<S>>] {
  const [value, setValue] = React.useState(propState || initialState)

  React.useEffect(() => {
    if (propState !== undefined && propState !== value) {
      setValue(propState)
    }
  }, [propState])

  return [value, setValue]
}
