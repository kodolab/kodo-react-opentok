import React from 'react'
import { usePrevious } from './usePrevious'
import isUndefined from 'lodash/isUndefined'
import has from 'lodash/has'
import keys from 'lodash/keys'
import get from 'lodash/get'
import isEqual from 'lodash/isEqual'
import isObjectLike from 'lodash/isObjectLike'
import entries from 'lodash/entries'
import mixin from 'lodash/mixin'

/**
 * Deep diff between two object-likes
 * @param  {Object} fromObject the original object
 * @param  {Object} toObject   the updated object
 * @return {Object}            a new object which represents the diff
 */
function deepDiff(fromObject: any, toObject: any) {
  const changes: { [key: string]: any } = {}

  const buildPath = (path: string | undefined, _obj: any, key: string) =>
    isUndefined(path) ? key : `${path}.${key}`

  const walk = (fromObject: any, toObject: any, path?: string) => {
    for (const key of keys(fromObject)) {
      const currentPath = buildPath(path, fromObject, key)
      if (!has(toObject, key)) {
        changes[currentPath] = { from: get(fromObject, key) }
      }
    }

    for (const [key, to] of entries(toObject)) {
      const currentPath = buildPath(path, toObject, key)
      if (!has(fromObject, key)) {
        changes[currentPath] = { to }
      } else {
        const from = get(fromObject, key)
        if (!isEqual(from, to)) {
          if (isObjectLike(to) && isObjectLike(from)) {
            walk(from, to, currentPath)
          } else {
            changes[currentPath] = { from, to }
          }
        }
      }
    }
  }

  walk(fromObject, toObject)

  return changes
}

// mom I'm on lodash
mixin({ deepDiff })

export const useDifferences = (value: any) => {
  const previousValue = usePrevious(value)

  const diff = React.useMemo(() => {
    return deepDiff(previousValue, value)
  }, [previousValue, value])

  return diff
}
