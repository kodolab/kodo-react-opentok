# kodo-react-opentok

> wrapper to opentok

[![NPM](https://img.shields.io/npm/v/kodo-react-opentok.svg)](https://www.npmjs.com/package/kodo-react-opentok) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save kodo-react-opentok
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'kodo-react-opentok'
import 'kodo-react-opentok/dist/index.css'

export const mcAuth = {
    "apiKey": "xxxxxx",
    "sessionId": "xxxxxxxxx",
    "token": "xxxxxxxxxxx",
   "type": "VONAGE",
}



const sessionProps = {
  apiKey: mcAuth.apiKey,
  sessionId: mcAuth.sessionId,
  token: mcAuth.token,
}

const BaseExample = () => {
  const [pub, setPub] = React.useState(false)
  
  return ( 
    <div>
      <h5>Session  {pub && <Button onClick={() => setPub(false)}>Close session</Button>} </h5>
      {pub ? (
        <OtProvider {...sessionProps}>
          <TestPublishVideo></TestPublishVideo>
          <ConnectionSatus />
          <UnconnectedComp />

          <TestOtPublisher />
              
          <hr />
          <h5>CustomRefOtPublisher</h5>
          <CustomRefOtPublisher />
      
          <hr />
          <h5>Publisher</h5>
          <TestPublisher />
      
          <StreamsList />
          <hr/>
          <SubscribersList/>
          <hr/>
        </OtProvider>
      ) : (
          <Button color='primary' onClick={() => setPub(true)}>Open session</Button>
        )}
    </div>
  )
}
```

## License

MIT © [pa0lin082](https://github.com/pa0lin082)
