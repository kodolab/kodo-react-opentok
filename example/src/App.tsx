import React from 'react'

import {
  // OtSession, 
  OtProvider,
  // useOtSession, 
  useOtStreams,
  useOtSubscribers,
  OtPublisher,
  OtSubscriber,
  useOtSessionState,
  Publisher,
  Subscriber
} from 'kodo-react-opentok'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { createMuiTheme,  ThemeProvider } from '@material-ui/core/styles';
import { orange, green } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button'

const theme = createMuiTheme({
  palette: {
    primary: orange,
    secondary: green,
  },
});



export const mcAuth = {
    "apiKey": "xxxxxx",
    "sessionId": "xxxxxxxxx",
    "token": "xxxxxxxxxxx",
   "type": "VONAGE",
}



const sessionProps = {
  apiKey: mcAuth.apiKey,
  sessionId: mcAuth.sessionId,
  token: mcAuth.token,
}

const UnconnectedComp = () => {
  // React.useContext(OtSessionContext)
  // console.log('########## render UnconnectedComp ')
  return (
    <h3>Unconnected con=mponent</h3>
  )
}


export const ConnectionSatus = () => {
  const status = useOtSessionState()
  // console.log('########## render ConnectionSatus',status )
  return (<pre>
    status:
    {JSON.stringify(status, null,2)}
  </pre>)
}


export const StreamsList = (props:{
  filterFn?: (s:any) => boolean
}) => {
  const {filterFn=(_) => true} = props
  const status = useOtStreams()
  // console.log('########## render StreamsList',status)

  const videoRef = React.useRef(null)
  const [stream, setStream] = React.useState<any>(undefined)
  return (
    <div>
      <h2>Stream list {status.state.streams.length}</h2>
    

      <hr />
      {stream && (
        <div>
          <h5>Selected stream: {stream?.id}</h5>

          <Subscriber 
          key={stream.id}
          stream={stream}
          ></Subscriber>
          {/* <OtSubscriber stream={stream}></OtSubscriber>

          <hr /> */}

          <OtSubscriber videoRef={videoRef} stream={stream}>
            <div style={{
              width: 100,
              height: 100,
              border: '2px solid red'
            }}>
              <div ref={videoRef} >

                <div style={{
                  position: 'absolute',
                  width: '100%',
                  height: '100%'
                }}>

                  video container
                  </div>
              </div>

            </div>


          </OtSubscriber>

        </div>
      )}
      <hr></hr>
      <div style={{height:200, overflow:'scroll'}}>
      {status.state.streams.filter(filterFn).map(s => (
        <div key={s.streamId}>
          <h5>[{(s as any).id}]{s.streamId}</h5>

          <Button onClick={() => {
            stream === s ? setStream(undefined) : setStream(s)
          }} > Show</Button>

        </div>
      ))}
       </div>


    </div>
  )
}


export const SubscribersList = () => {
  const status = useOtSubscribers()
  // console.log('########## render SubscribersList')
  // const [stream, setStream] = React.useState<any>(undefined)
  return (
    <div>
      <h2>Subscribers list</h2>
      {status.state.subscribers.map(s => (
        <div key={s.id}>
          <h5>{s.id}</h5>


        </div>
      ))}



    </div>
  )
}

export const TestOtPublisher = (_: any) => {
  const [pub, setPub] = React.useState(false)

  return (
    <div>
      <h5>Custom ref publisher</h5>
      {pub ? (
        <div>
          <OtPublisher >
          </OtPublisher>
          <Button onClick={() => setPub(false)}>Unpub</Button>
        </div>
      ) : (
          <Button onClick={() => setPub(true)}>pub</Button>
        )}
    </div>
  )

}

export const CustomRefOtPublisher = (_: any) => {
  const [pub, setPub] = React.useState(false)

  const videoRef1 = React.useRef(null)
  const videoRef2 = React.useRef(null)

  return (
    <div>
      <h5>Custom ref publisher</h5>
      <div style={{
              width: 100,
              height: 100,
              border: '2px solid red'
            }}
           
            >

              <div ref={videoRef1} > 
              video container
              </div>
            </div>

            <div 
            ref={videoRef2}
            style={{
              width: 100,
              height: 100,
              border: '2px solid red'
            }}
           
            >

             
            </div>

      {pub ? (
        <div>
          <OtPublisher videoRef={videoRef1} insertMode='replace' >
            <h6>Publisher content</h6>
          </OtPublisher>

          <OtPublisher videoRef={videoRef2} insertMode='replace'>
            <h6>Publisher content</h6>
          </OtPublisher>
          <Button onClick={() => setPub(false)}>Unpub</Button>
        </div>
      ) : (
          <Button onClick={() => setPub(true)}>pub</Button>
        )}
    </div>
  )

}

export const TestPublisher = (props: {open?:boolean}) => {
  const {open=false} = props
  const [pub, setPub] = React.useState(open)

  return (
    <div>
      <h5>Publisher</h5>
      {pub ? (
        <div>
          <Publisher onClose={() => console.log('onClose')} />
          <Button onClick={() => setPub(false)}>Unpub</Button>
        </div>
      ) : (
          <Button onClick={() => setPub(true)}>pub</Button>
        )}
    </div>
  )

}


export const TestPublishVideo = () =>{

  // <video id="video" crossOrigin="anonymous" src="https://iandevlin.github.io/mdn/video-player-with-captions/video/sintel-short.mp4" width="320" height="240" controls></video>

  const [pub, setPub] = React.useState(false)
  const [stream, setStream] = React.useState()

  React.useEffect(() => {
    const video = document.querySelector('#video');
  if (!(video as any)?.captureStream) {
    console.error('This browser does not support VideoElement.captureStream(). You must use Google Chrome.');
    return;
  }
  const stream = (video as any).captureStream();
  // console.log('stream',stream)

  // const videoTracks = stream.getVideoTracks();
    // const audioTracks = stream.getAudioTracks();
    setStream(stream)


  },[])

  return (
    <div>
      <h5>Publisher Video</h5>
      {pub ? (
        <div>
          <Publisher 
          onClose={() => console.log('onClose')} 
          videoSource={(stream as any)?.getVideoTracks()[0]}
          />
          <Button onClick={() => setPub(false)}>Unpub</Button>
        </div>
      ) : (
          <Button onClick={() => setPub(true)}>pub</Button>
        )}
    </div>
  )
}





const BaseExample = () => {
  const [pub, setPub] = React.useState(false)
  
  return ( 
    <div>
      <h5>Session  {pub && <Button onClick={() => setPub(false)}>Close session</Button>} </h5>
      {pub ? (
        <OtProvider {...sessionProps}>
          <TestPublishVideo></TestPublishVideo>
          <ConnectionSatus />
          <UnconnectedComp />

          <TestOtPublisher />
              
          <hr />
          <h5>CustomRefOtPublisher</h5>
          <CustomRefOtPublisher />
      
          <hr />
          <h5>Publisher</h5>
          <TestPublisher />
      
          <StreamsList />
          <hr/>
          <SubscribersList/>
          <hr/>
        </OtProvider>
      ) : (
          <Button color='primary' onClick={() => setPub(true)}>Open session</Button>
        )}
    </div>
  )
}


const PublishBeforeConnect = () => {
  const [pub, setPub] = React.useState(false)
  
  return ( 
    <div>
      <h5>Session  {pub && <Button onClick={() => setPub(false)}>Close session</Button>} </h5>
      {pub ? (
        <OtProvider {...sessionProps}>
          <div style={{width:'30%', float:'left'}}>
            <TestPublisher open />
          </div>
          <div style={{width:'30%', float:'left'}}>
            <StreamsList />
          </div>
          <div style={{width:'30%', float:'left'}}>
            <ConnectionSatus />
          </div>

      
          
        
        </OtProvider>
      ) : (
          <Button onClick={() => setPub(true)}>Open session</Button>
        )}
    </div>
  )
}


const TestPlaceholdersImages = () => {
  const subscriberPlaceholderImageUriList= ["https://mdp-backend-dev-paolo.s3.amazonaws.com/media/original_files/LOGO_EXPERT_PEOPLE.png"]


  return ( 
    <div>
      <OtProvider {...sessionProps} subscriberPlaceholderImageUriList={subscriberPlaceholderImageUriList}>
        <h5>TestPlaceholdersImages </h5>


            <div style={{width:'30%', float:'left'}}>
              <TestPublisher open={false} />
            </div>
            <div style={{width:'30%', float:'left'}}>
              <ConnectionSatus />
            </div>
            <div style={{width:'30%', float:'left'}}>
              <StreamsList filterFn={(s) => {
                console.log('s',s)
                return !s.hasVideo
                }} />
            </div>
  </OtProvider>
    </div>
  )
}



const TestCommonPublisher = () => {

  const [pub, setPub] = React.useState(false)


  return ( 
    <div>
      <OtProvider {...sessionProps} >
        <h5>TestPlaceholdersImages </h5>
            <div style={{width:'30%', float:'left'}}>
             
            {pub ? (
              <div>
                <OtPublisher
                insertDefaultUI={false}
                onClose={() => console.log('onClose')} />
                <Button onClick={() => setPub(false)}>Unpub</Button>
              </div>
            ) : (
                <Button onClick={() => setPub(true)}>pub</Button>
              )}
             


            </div>
            <div style={{width:'30%', float:'left'}}>
              <ConnectionSatus />
            </div>
            <div style={{width:'30%', float:'left'}}>
              <StreamsList filterFn={(s) => {
                console.log('s',s)
                return !s.hasVideo
                }} />
            </div>
  </OtProvider>
    </div>
  )
}




const TestSubscriberListEvents= () => {

  const [logs, setLogs] = React.useState<string[]>([
    'initial logs'
  ])
  const [sub, setSub] = React.useState(false)
  

  const log = React.useCallback((msg:string) => setLogs(old => [...old,msg]) ,[])


  const clbMap = React.useMemo(() => {
    return {
      'videoElementCreated': (obj?:any) => {
        console.log('OtSubscriber videoElementCreated',obj)
        log(`OtSubscriber videoElementCreated `)
      },
      'streamCreated': (obj?:any) => {
        console.log('OtSubscriber streamCreated',obj)
        log(`OtSubscriber streamCreated`)
      }
    }
  },[log])
 
  const status = useOtStreams()

  return ( 
    <div>

  
    <div style={{height:200, overflow:'scroll'}}>
      {logs.map(s => <p>{s}</p> )}
    </div>  
    <div style={{height:600, overflow:'scroll'}}>
    {sub ? (
              <div>
                <Button onClick={() => setSub(false)}>sub</Button>

              </div>
      
          
            ) : (
                <Button onClick={() => setSub(true)}>sub</Button>
              )}

      
      <h1>Stream list {status.state.streams.length}</h1>
      {status.state.streams.map(s => { 
        console.log('s',s)
        return(
        <div style={{width:'200', height:'100'}}>
        <h1> stream: {s.streamId}</h1>

        {sub &&  <OtSubscriber 
          insertDefaultUI={false} 
          key={s.streamId} stream={s} on={clbMap as any}/>}

         
        </div>
        )})}
    </div>
    </div>
  )
}


const TestSubscriberEvents = () => {

  const [pub, setPub] = React.useState(false)
  const [sub, setSub] = React.useState(true)
  


  return ( 
    <div>
      <OtProvider {...sessionProps} >
        <h5>TestSubscriberEvents </h5>
            <div style={{width:'30%', float:'left'}}>
             
            {pub ? (
              <div>
                <OtPublisher
                // insertDefaultUI={true}
                onClose={() => console.log('onClose')} />
                <Button onClick={() => setPub(false)}>Unpub</Button>
              </div>
            ) : (
                <Button onClick={() => setPub(true)}>pub</Button>
              )}
             
            </div>
            {/* <div style={{width:'30%', float:'left'}}> */}
              {/* <ConnectionSatus /> */}
              {/* <StreamsList /> */}
            {/* </div> */}
            <div style={{width:'30%', float:'left'}}>

            {sub ? (
              <div>
                <Button onClick={() => setSub(false)}>sub</Button>
                 <div style={{height:200, overflow:'scroll'}}>
                <TestSubscriberListEvents/>
              </div>
      
              </div>
            ) : (
                <Button onClick={() => setSub(true)}>sub</Button>
              )}


             
            </div>
  </OtProvider>
    </div>
  )
}


// const exampleComponents = {
//   '/': BaseExample,
//   'publishBeforeSession': PublishBeforeConnect,
// }

const App = () => {
  // const [pub, setPub] = React.useState(false)

  return (
    <ThemeProvider theme={theme}>

   
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/publishBeforeSession">Test publish before session</Link>
            </li> 
            <li>
              <Link to="/TestPlaceholdersImages">TestPlaceholdersImages</Link>
            </li>
            <li>
              <Link to="/commonpublisher">commonpublisher</Link>
            </li> 
            <li>
              <Link to="/TestSubscriberEvents">TestSubscriberEvents</Link>
            </li>
           
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/commonpublisher">
            <TestCommonPublisher />
          </Route> 
          <Route path="/publishBeforeSession">
            <PublishBeforeConnect />
          </Route> 
          <Route path="/TestPlaceholdersImages">
            <TestPlaceholdersImages />
          </Route> 
          <Route path="/TestSubscriberEvents">
            <TestSubscriberEvents />
          </Route>
          <Route path="/">
            <BaseExample />
          </Route>
        </Switch>
      </div>
    </Router>
    </ThemeProvider>
  )
  
}

export default App
